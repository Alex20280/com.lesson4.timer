package layouts.sourceit.com.comhomework5timer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyFragment extends Fragment {

    private static final String KEY = "key";

    public static MyFragment newInstance (String text) {
        Bundle args = new Bundle();
        args.putString(KEY, text);
        MyFragment fragment = new MyFragment();
        fragment.setArguments(args);
        return fragment;
    }
    String value;

    @BindView(R.id.text)
    TextView text;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null && getArguments().containsKey(KEY)){
            value = getArguments().getString(KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.my_fragment, container,false);
        ButterKnife.bind(this, root);
        text.setText(value);
        return root;
    }
}
