package layouts.sourceit.com.comhomework5timer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.OnClick;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.timer)
    void onFirstClick(){
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.from_left, R.anim.to_right)
                .replace(R.id.navigable_container, MyFragment.newInstance("Fragment 1"))
                .commit();

    }
    @OnClick(R.id.stopWatch)
    void onSecondClick(){
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.from_right, R.anim.to_left)
                .replace(R.id.navigable_container, MyFragment.newInstance("Fragment 2"))
                .commit();

    }

}
